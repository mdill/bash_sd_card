# BASH SD card backup/restore script

## Purpose

This BASH script is designed to help streamline the backup and restore of
external media.  It is written explicitly for SD cards, but will work with any
disk partition in Linux.

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/bash_sd_card.git

## Backup

The backup function will take two arguments: one for the input file and one for
the output filename.  It will then append a date to the output filename, and
compress the image it copies off of the media.

## Restore

The restore function will take two arguments: one for an image filename, and one
for a drive destination to write to.  It will automatically decompress the image
file before writing to the destination.

## Placement

It is suggested that these scripts be placed in `/usr/local/bin/` for simplicity
and ease-of-use.  Once this is done, they can be made executable by:

    $ chmod +x /usr/local/bin/{backupsd,restoresd,copydd}

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/bash_sd_card/src/89cca83e78e8f252a62ff4ad8c9535ec2234d9ed/LICENSE.txt?at=master) file for
details.

